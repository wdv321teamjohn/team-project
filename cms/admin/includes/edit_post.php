<?php

if(isset($_GET['p_id'])){

	$the_post_id = $_GET['p_id'];

}
	$query = "SELECT * FROM posts WHERE post_id = $the_post_id";
    $select_posts = mysqli_query($connection, $query);

    while($row = mysqli_fetch_assoc($select_posts)){

        $post_id = $row['post_id'];
        $post_author = $row['post_author'];
        $post_title = $row['post_title'];
        $post_category_id = $row['post_category_id'];
        $post_status = $row['post_status'];
        $post_image = $row['post_image'];
        $post_content = $row['post_content'];
        $post_tag = $row['post_tag'];
        $post_comment_count = $row['post_comment_count'];
        $post_date = $row['post_date'];


	}

	if(isset($_POST['update_post'])){
		
		$post_author = $_POST['post_author'];
        $post_title = $_POST['post_title'];
        $post_category_id = $_POST['post_category_id'];
        $post_status = $_POST['post_status'];
        $post_image = $_FILES['image']['name'];
        $post_image_temp = $_FILES['image']['tmp_name'];
        $post_content = $_POST['post_content'];
        $post_tag = $_POST['post_tag'];

        move_uploaded_file($post_image_temp, "../images/$post_image");

        if(empty($post_image)){
        	$query = "SELECT * FROM posts WHERE post_id = $the_post_id";

        	$select_image = mysqli_query($connection, $query);

        	while($row = mysqli_fetch_array($select_image)){

        		$post_image = $row['post_image'];

        	}
        }


        $sql = "UPDATE posts SET " ;
		$sql .= "post_title=?, ";
		$sql .= "post_category_id=?, ";
		$sql .= "post_date=?, ";
		$sql .= "post_author=?, ";
		$sql .= "post_status=?, ";
		$sql .= "post_tag=?, ";
		$sql .= "post_content=?, ";
		$sql .= "post_image=? ";
		$sql .= " WHERE (post_id='$the_post_id')";

		$query = $connection->prepare($sql);
	
		$query->bind_param('sissssss',$post_title,$post_category_id,$post_date,$post_author,$post_status,$post_tag,$post_content,$post_image);

		$post_date = date("Y-m-d", strtotime($post_date));

		if ( $query->execute() )
		{
			echo "<h1>Your record has been successfully UPDATED the database.</h1>";
		}
		else
		{
			$message .= "<h2 style='color:red'>" . mysqli_error($connection) . "</h2>";
		}
	}
	else	
	{
		
		$update = $_GET['p_id'];	
		
		echo "<h1>update: $update</h1>";
		
		
		$sql = "SELECT post_title,post_category_id,post_date,post_author,post_status,post_tag,post_content,post_image FROM posts WHERE post_id=?";	
	
		$query = $connection->prepare($sql);
		
		$query->bind_param("i",$update);	
	
		if( $query->execute() )	//Run Query and Make sure the Query ran correctly
		{
			$query->bind_result($post_title,$post_category_id,$post_date,$post_author,$post_status,$post_tag,$post_content,$post_image);
		
			$query->store_result();
			
			$query->fetch();
		}
		else
		{
			$message .= "<h2>" . mysqli_error($connection) . "</h2>" ;			
		}
	
	}//end else submitted
?>




<form action="" method="post" enctype="multipart/form-data">
	
	<div class="form-group">
		<label for="title">Post Title</label>
		<input value="<?php echo $post_title; ?>" type="text" class="form-control" name="post_title">
	</div>

	<div class="form-group">
		<label for="post_category">Post Category Id</label>
		<input value="<?php echo $post_category_id; ?>" type="text" class="form-control" name="post_category_id">
	</div>

	<div class="form-group">
		<select name="post_category" id="">
			
		<?php

			$query = "SELECT * FROM categories";
	        $select_categories = mysqli_query($connection, $query);

	        confirm($select_categories);

	        while($row = mysqli_fetch_assoc($select_categories)){
	            $cat_id = $row['cat_id'];
	            $cat_title = $row['cat_title'];

	            echo "<option value=''>{$cat_title}</option>";
	        }


		?>

		</select>
	</div>

	<div class="form-group">
		<label for="author">Post Author</label>
		<input value="<?php echo $post_author; ?>" type="text" class="form-control" name="post_author">
	</div>

	<div class="form-group">
		<label for="post_status">Post Status</label>
		<input value="<?php echo $post_status; ?>" type="text" class="form-control" name="post_status">
	</div>

	<div class="form-group">
		<img width="100" src="../images<?php echo $post_image ?>" alt="">
		<input type="file" name="image">
	</div>

	<div class="form-group">
		<label for="post_tags">Post Tags</label>
		<input value="<?php echo $post_tag; ?>" type="text" class="form-control" name="post_tag">
	</div>

	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea type="text" class="form-control" name="post_content" id="" cols="30" rows="10">
			<?php echo $post_content; ?>
		</textarea>
	</div>

	<div class="form-group">
		<input class="btn btn-primary" type="submit" name="update_post" value="Update Post">
	</div>
</form>